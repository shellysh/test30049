<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    public static function tableName(){
		return 'user';
	}
	
	public function rules(){
		return 
		[
			[['username','password','authKey','name'],'string','max' => 255],
			[['username','password'],'required'],
			[['username'],'unique'],
		];
	}

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
		return self::findOne($id);
        //return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		throw new NotSupportedException('Not supported');
		return null;
        /*foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;*/
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
		return self::findOne(['username' => $username]);
        /*foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;*/
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */	
	public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	
	//hash password before saving a new user
    public function beforeSave($insert) 
    {
        $return = parent::beforeSave($insert);
		//בדיקה האם הסיסמה השתנתה - אירוע של עדכון
        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password); //ביצוע קידוד - HASH
		//בודק האם מדובר במשתמש חדש, ומטרתו לעדכן את המפתח
        if ($this->isNewRecord)
		    $this->authKey = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	
	public static function getAuthors()
	{
		$allAuthors = self::find()->all();
		$allAuthorsArray = ArrayHelper::
					map($allAuthors, 'id', 'name');
		return $allAuthorsArray;						
	}
}
