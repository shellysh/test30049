<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use app\models\Status;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

    <?php //$form->field($model, 'category')->textInput() ?>
	<?= $form->field($model, 'category')->
				dropDownList(Category::getCategorys()) ?> 

    <?php //$form->field($model, 'author')->textInput() ?>
	<?= $form->field($model, 'author')->
				dropDownList(User::getAuthors()) //,['disabled' => !\Yii::$app->user->can('changeAuthor')] ?> 

    <?php //$form->field($model, 'status')->textInput() ?>
	<?= $form->field($model, 'status')->
				dropDownList(Status::getStatuses()) ?> 

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
