<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170716_134040_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
				'id'=> $this->primaryKey(),
				'username' => $this->string()->notNull(),
				'name' => $this->string()->notNull(),
				'password' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
