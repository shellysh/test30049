<?php

use yii\db\Migration;

class m170716_140516_add_authkey_column extends Migration
{
    public function safeUp()
    {
		$this->addColumn('user', 'authKey', $this->string()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'authKey');
    }
}
